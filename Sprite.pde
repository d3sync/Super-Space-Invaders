class Sprite{

  PImage sprite;
  float scale;
  float scaleFactor;

  public Sprite(String name, float scale){

    sprite = loadImage(name + ".png");
    this.scale = scale;
  }

  public Sprite(String name, int scale){
    this(name, (float)scale);
  }

  void display(float x, float y){
    pushMatrix();

    scaleFactor = this.scale / sprite.width;
    translate(x,y);
    imageMode(CENTER);
    scale(scaleFactor);
    image(sprite, 0,0);

    popMatrix();

  }


  float getWidth(){
    return sprite.width;
  }

  float getHeight(){
    return sprite.height;
  }

  float getScaledWidth(){
    return sprite.width * scaleFactor;
  }

  float getScaledHeight(){
    return sprite.height * scaleFactor;
  }
}