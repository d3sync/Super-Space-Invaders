class Alien {
  PVector position;
  PVector velocity;
  PVector acceleration;
  int size = 30;
  boolean side = false;
  int shoot;
  PVector shotSpeed;
  PVector shotSpeed2;
  ArrayList<Alien> others;
  ArrayList<Laser> returnFire;

  Sprite enemy;
  int numInRow;

  public Alien(float x, float y, ArrayList<Alien> aliensArray, ArrayList<Laser> lasersArray, ArrayList<Laser> returnFire, int numInRow, int shoot) {

    position = new PVector(x, y);
    velocity = new PVector(0.5, 0);
    acceleration = new PVector(0, 0);
    shotSpeed = new PVector(2,5);
    shotSpeed2 = new PVector(-2,5);
    others = aliensArray;
    lasers = lasersArray;
    this.returnFire = returnFire;
    this.shoot = shoot;

    this.numInRow = numInRow;
    //pick random enemy sprite
    int enemyNum = (int)random(1,5);
    enemy = new Sprite("enemy" + enemyNum, 50 -( numInRow * 2 ));
  }

  void move() {
    //check the side walls, and reverse direction
    if (checkSide()) {
      position.y += 20;
      velocity.x *= -1;
    } else {
      velocity.y = 0;
    }

    //make it so...
    velocity.add(acceleration);
    position.add(velocity);
  }

  void display() {

    enemy.display(position.x, position.y);
  }

  boolean checkSide() {
    //return true if hit the wall
    if (position.x > width - (enemy.getScaledWidth() / 2) || position.x < 0 + (enemy.getScaledWidth() / 2) || collide()) {
      if(shoot > 0){
        println(shoot);
        pew();
      }
      return true;
    } else {
      return false;
    }
  }


  boolean collide() {
    //check for collisions
    boolean hitSide = false;

    //check if any other aliens has hit a side wall, if so return true
    for (int i = 0; i < others.size(); i++) {
      if (others.get(i).getPos().x < (enemy.getScaledWidth() / 2) || others.get(i).getPos().x > width - (others.get(i).getScaledWidth() / 2)) {
        return true;
      }
    }
    return false;
  }

  boolean shot() {

    //if hit by laser, return true and remove laser from arraylist. its long and confusing, but it works
    for (int i = lasers.size() - 1; i >=0; i--) {
              //check right                                     check left
      if ((lasers.get(i).getPos().x > position.x - (enemy.getScaledWidth() /2)) && (lasers.get(i).getPos().x < position.x + (enemy.getScaledWidth() /2)) &&
              //check bottom                                    check top
        (lasers.get(i).getPos().y > position.y - (enemy.getScaledHeight() / 2)) && (lasers.get(i).getPos().y < position.y + (enemy.getScaledHeight() / 2))) {

          lasers.remove(i);
          return true;
      }
    }

    return false;
  }
  
  void pew(){
    if(shoot > 0){
      returnFire.add(new Laser(this.getPos().x, this.getPos().y, this.getShotSpeed()));
      returnFire.add(new Laser(this.getPos().x, this.getPos().y, this.getShotSpeed2()));
      
    }
    
  }
  

  PVector getPos() {
    return position;
  }

  float getWidth(){
    return enemy.getWidth();
  }

  float getHeight(){
    return enemy.getHeight();
  }

  float getScaledWidth(){
    return enemy.getScaledWidth();
  }

  float getScaledHeight(){
    return enemy.getScaledHeight();
  }
  PVector getShotSpeed(){
   return shotSpeed; 
  }
  PVector getShotSpeed2(){
   return shotSpeed2; 
  }
  
  void setSpeed(float speed) {
    //if one alien gets hit, the rest speed up. speed is costrained as to not bug out
    if (velocity.x < 3 && velocity.x > -3) {
      velocity.x *= speed;
    }
  }
}