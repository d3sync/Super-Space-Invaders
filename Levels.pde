static class Levels {
  
  static int[] level1(){
    int numInRow = 3;
    int numInRowMid = numInRow - 1;
    int numOfAliens = 12;
    int shoot = 0;
  
    int[] temp = {numInRow, numInRowMid, numOfAliens, shoot};
    return temp;
  }
  
  static int[] level2(){
    int numInRow = 4;
    int numInRowMid = numInRow - 1;
    int numOfAliens = 15;
    int shoot = 0;
  
    int[] temp = {numInRow, numInRowMid, numOfAliens, shoot};
    return temp;
  }
  
  static int[] level3(){
    int numInRow = 6;
    int numInRowMid = numInRow - 1;
    int numOfAliens = 24;
    int shoot = 0;

    int[] temp = {numInRow, numInRowMid, numOfAliens, shoot};
    return temp;
  }
  
  static int[] level4(){
    int numInRow = 8;
    int numInRowMid = numInRow - 1;
    int numOfAliens = 34;
    int shoot = 0;
  
    int[] temp = {numInRow, numInRowMid, numOfAliens, shoot};;
    return temp;
  }
  
  static int[] level5(){
    int numInRow = 10;
    int numInRowMid = numInRow - 1;
    int numOfAliens = 45;
    int shoot = 2;
  
    int[] temp = {numInRow, numInRowMid, numOfAliens, shoot};
    return temp;
  }
  

  
  static int[] getLevel(int level){
    if (level == 1){
      return level1();
    }
    else if (level == 2){
      return level2();
    }
    else if (level == 3){
      return level3();
    }
    else if (level == 4){
      return level4();
    }
    else if (level == 5){
      return level5();
    }
    
    else{
      return level1();
    }
    
  }
  
  
}