//int x = 10;
//int y = 10;
boolean rightKey;
boolean leftKey;
boolean side = false;
int level = 4;

int[] vars = Levels.getLevel(level);
int numInRow = vars[0];
int numInRowMid = vars[1];
int numOfAliens = vars[2];
int shoot = vars[3];


Ship player;
ArrayList<Alien> aliens = new ArrayList<Alien>();
ArrayList<Laser> lasers = new ArrayList<Laser>();
ArrayList<Laser> returnFire = new ArrayList<Laser>();

void setup() {
  size(900, 800);

  player = new Ship(width/2, height - 40);

}


void draw() {
  background(0);


  //ship
  player.move(rightKey, leftKey);
  player.display();

  //lasers
  for (int i = lasers.size() - 1; i >=0; i--) {
    lasers.get(i).display();
    lasers.get(i).move();
    
  }
  
  //return fire from alien ships
  for (int i = returnFire.size() - 1; i >=0; i--) {
    returnFire.get(i).display();
    returnFire.get(i).move();
    
  }

  //alien

  //this loop itterate backwards due to how elements are removed from an array. basically avoids disaster.
  for (int i = aliens.size() -1; i >= 0; i--) {
    aliens.get(i).move();
    aliens.get(i).display();

    if (aliens.get(i).shot()) {
      for (int j = 0; j < aliens.size(); j++) {
        aliens.get(j).setSpeed(1.07);
      }
      aliens.remove(i);
      aliens.trimToSize();
    }
  }
  
  if(aliens.size() < 1){
    nextLevel();
  }
}


void keyPressed() {

  if (keyCode == RIGHT) {
    rightKey = true;
  }
  if (keyCode == LEFT) {
    leftKey = true;
  }
  if (keyCode == 32) {
    
    lasers.add(new Laser(player.getPos().x, player.getPos().y, player.getShotSpeed()));
  }
}

void keyReleased() {

  if (keyCode == RIGHT) {
    rightKey = false;
  }
  if (keyCode == LEFT) {
    leftKey = false;
  }
}

void nextLevel() {
  // start next level
  vars = Levels.getLevel(++level);
  numInRow = vars[0];
  numInRowMid = vars[1];
  numOfAliens = vars[2];
  shoot = vars[3];
  
  int row = 40;
  int col = 0;
  boolean offsetting = false;

  for (int i = 0; i < numOfAliens; i++) {
    int offset = 0;
    int shootRow = 0;
    
    if (i > numOfAliens - numInRow){
      shootRow = 1;
    }
    
    
    
    
    if ( i % numInRowMid == 0) {
      col = 1;
      row += 40;
      offsetting = !offsetting;
    } else {
      col++;
    }
    if (offsetting) {
      offset = ((width)/numInRow)/2;
    }
  
    aliens.add(new Alien((width/numInRow) * col + offset, row, aliens, lasers, returnFire, numInRow, shootRow));
  }
  
  if(level == 5){
    level = 0;
  }
}