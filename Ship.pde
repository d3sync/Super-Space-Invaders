class Ship {

  PVector position;
  PVector velocity;
  PVector acceleration;
  PVector shotSpeed;
  Sprite player;

  Ship(int x, int y) {

    this.position = new PVector(x, y);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    player = new Sprite("player1", 50.0);
    shotSpeed = new PVector(0, -5);
  }

  void move(boolean rightKey, boolean leftKey ) {

    if (position.x < (player.getWidth() * -1) /2) {
      position.x = width + 15;
    } else if (position.x > width + 15) {
      position.x = -15;
    }
    //right arrow, move right
    if (rightKey) {
      acceleration.x += 0.05;
    }  //left arrow, move left
    else if (leftKey) {
      acceleration.x -= 0.05;
    }
    else {
      //no arrow pressed, slow down till stopped
      //this is super hacky. there must be a way to do this with like acceleration in the oposite direction??
      if (velocity.x > 0.001) {
        velocity.x -= abs(velocity.x) * 0.1;
        acceleration.x = 0;
      }
      if ( velocity.x < -0.001) {
        velocity.x += abs(velocity.x) * 0.1;
        acceleration.x = 0;
      }
    }
    //do magic
    velocity.add(acceleration);
    velocity.x = constrain(velocity.x, -20, 20);
    position.add(velocity);
  }

  void display() {

    player.display(position.x, position.y);


  }



  boolean colide() {
    //TODO
    return false;
  }

  PVector getPos() {
    return position;
  }
  
  PVector getShotSpeed(){
    return shotSpeed;
  }
}