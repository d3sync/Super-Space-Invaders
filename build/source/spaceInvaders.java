import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class spaceInvaders extends PApplet {

//int x = 10;
//int y = 10;
boolean rightKey;
boolean leftKey;
boolean side = false;
int numInRow = 9;
int numInRowMid = numInRow - 1;
int numOfAliens = 40;


Ship player;
ArrayList<Alien> aliens = new ArrayList<Alien>();
ArrayList<Laser> lasers = new ArrayList<Laser>();

public void setup() {
  

  player = new Ship(width/2, height - 40);
  int row = 40;
  int col = 0;
  boolean offsetting = false;
  int fillCol = color((int)random(255), (int)random(255), (int)random(255));

  for (int i = 0; i < numOfAliens; i++) {
    int offset = 0;

    if ( i % numInRowMid == 0) {
      col = 1;
      row += 40;
      offsetting = !offsetting;
    } else {
      col++;
    }
    if (offsetting) {
      offset = ((width)/numInRow)/2;
    }

    aliens.add(new Alien((width/numInRow) * col + offset, row, aliens, lasers, numInRow));
  }
}


public void draw() {
  background(0);


  //ship
  player.move(rightKey, leftKey);
  player.display();

  //lasers
  for (int i = lasers.size() - 1; i >=0; i--) {
    lasers.get(i).display();
    lasers.get(i).move();
  }

  //alien

  //this loop itterate backwards due to how elements are removed from an array. basically avoids disaster.
  for (int i = aliens.size() -1; i >= 0; i--) {
    aliens.get(i).move();

    aliens.get(i).display();

    if (aliens.get(i).shot()) {
      for (int j = 0; j < aliens.size(); j++) {
        aliens.get(j).setSpeed(1.07f);
      }
      aliens.remove(i);
      aliens.trimToSize();
    }
  }
}
public void keyPressed() {

  if (keyCode == RIGHT) {
    rightKey = true;
  }
  if (keyCode == LEFT) {
    leftKey = true;
  }
  if (keyCode == 32) {
    lasers.add(new Laser(player.getPos().x, player.getPos().y));
  }
}

public void keyReleased() {

  if (keyCode == RIGHT) {
    rightKey = false;
  }
  if (keyCode == LEFT) {
    leftKey = false;
  }
}
class Alien {
  PVector position;
  PVector velocity;
  PVector acceleration;
  int size = 30;
  boolean side = false;
  ArrayList<Alien> others;

  Sprite enemy;
  int numInRow;

  public Alien(float x, float y, ArrayList<Alien> aliensArray, ArrayList<Laser> lasersArray, int numInRow) {

    position = new PVector(x, y);
    velocity = new PVector(0.5f, 0);
    acceleration = new PVector(0, 0);
    others = aliensArray;
    lasers = lasersArray;

    this.numInRow = numInRow;
    //pick random enemy sprite
    int enemyNum = (int)random(1,5);
    enemy = new Sprite("enemy" + enemyNum, 50 -( numInRow * 2 ));
  }

  public void move() {
    //check the side walls, and reverse direction
    if (checkSide()) {
      position.y += 20;
      velocity.x *= -1;
    } else {
      velocity.y = 0;
    }

    //make it so...
    velocity.add(acceleration);
    position.add(velocity);
  }



  public void display() {

    enemy.display(position.x, position.y);


  }

  public boolean checkSide() {
    //return true if hit the wall
    if (position.x > width - (enemy.getScaledWidth() / 2) || position.x < 0 + (enemy.getScaledWidth() / 2) || collide()) {
      return true;
    } else {
      return false;
    }
  }


  public boolean collide() {
    //check for collisions
    boolean hitSide = false;

    //check if any other aliens has hit a side wall, if so return true
    for (int i = 0; i < others.size(); i++) {
      if (others.get(i).getPos().x < (enemy.getScaledWidth() / 2) || others.get(i).getPos().x > width - (others.get(i).getScaledWidth() / 2)) {
        return true;
      }
    }
    return false;
  }

  public boolean shot() {

    //if hit by laser, return true and remove laser from arraylist. its long and confusing, but it works
    for (int i = lasers.size() - 1; i >=0; i--) {
              //check right                                     check left
      if ((lasers.get(i).getPos().x > position.x - (enemy.getScaledWidth() /2)) && (lasers.get(i).getPos().x < position.x + (enemy.getScaledWidth() /2)) &&
              //check bottom                                    check top
        (lasers.get(i).getPos().y > position.y - (enemy.getScaledHeight() / 2)) && (lasers.get(i).getPos().y < position.y + (enemy.getScaledHeight() / 2))) {

          lasers.remove(i);
          return true;
      }
    }

    return false;
  }

  public PVector getPos() {
    return position;
  }

  public float getWidth(){
    return enemy.getWidth();
  }

  public float getHeight(){
    return enemy.getHeight();
  }

  public float getScaledWidth(){
    return enemy.getScaledWidth();
  }

  public float getScaledHeight(){
    return enemy.getScaledHeight();
  }
  public void setSpeed(float speed) {
    //if one alien gets hit, the rest speed up. speed is costrained as to not bug out
    if (velocity.x < 3 && velocity.x > -3) {
      velocity.x *= speed;
    }
  }
}
class Laser {
  PVector position;
  PVector velocity;

  public Laser(float x, float y) {
    position = new PVector(x, y);
    velocity = new PVector(0, -5);
  }

  public void display() {
    stroke(255);
    strokeWeight(4);
    point(position.x, position.y);
  }

  public void move() {
    position.add(velocity);
  }

  public PVector getPos() {
    return position;
  }
}
class Ship {

  PVector position;
  PVector velocity;
  PVector acceleration;
  Sprite player;

  Ship(int x, int y) {

    this.position = new PVector(x, y);
    this.velocity = new PVector(0, 0);
    this.acceleration = new PVector(0, 0);
    player = new Sprite("player1", 50.0f);
  }

  public void move(boolean rightKey, boolean leftKey ) {

    if (position.x < (player.getWidth() * -1) /2) {
      position.x = width + 15;
    } else if (position.x > width + 15) {
      position.x = -15;
    }
    //right arrow, move right
    if (rightKey) {
      acceleration.x += 0.05f;
    }  //left arrow, move left
    else if (leftKey) {
      acceleration.x -= 0.05f;
    }
    else {
      //no arrow pressed, slow down till stopped
      //this is super hacky. there must be a way to do this with like acceleration in the oposite direction??
      if (velocity.x > 0.001f) {
        velocity.x -= abs(velocity.x) * 0.1f;
        acceleration.x = 0;
      }
      if ( velocity.x < -0.001f) {
        velocity.x += abs(velocity.x) * 0.1f;
        acceleration.x = 0;
      }
    }
    //do magic
    velocity.add(acceleration);
    velocity.x = constrain(velocity.x, -20, 20);
    position.add(velocity);
  }

  public void display() {
    // ship

    // fill(127, 127);
    // strokeWeight(1);
    // stroke(255);
    // ellipse(position.x, position.y, 30, 30);
    player.display(position.x, position.y);


  }



  public boolean colide() {
    //TODO
    return false;
  }

  public PVector getPos() {
    return position;
  }
}
class Sprite{

  PImage sprite;
  float scale;
  float scaleFactor;

  public Sprite(String name, float scale){

    sprite = loadImage("PNG/" + name + ".png");
    this.scale = scale;
  }

  public Sprite(String name, int scale){
    this(name, (float)scale);
  }

  public void display(float x, float y){
    pushMatrix();

    scaleFactor = this.scale / sprite.width;
    translate(x,y);
    imageMode(CENTER);
    scale(scaleFactor);
    image(sprite, 0,0);

    popMatrix();

  }


  public float getWidth(){
    return sprite.width;
  }

  public float getHeight(){
    return sprite.height;
  }

  public float getScaledWidth(){
    return sprite.width * scaleFactor;
  }

  public float getScaledHeight(){
    return sprite.height * scaleFactor;
  }
}
  public void settings() {  size(900, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "spaceInvaders" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
